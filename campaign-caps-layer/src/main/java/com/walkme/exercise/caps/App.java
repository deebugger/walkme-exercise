package com.walkme.exercise.caps;

import io.vertx.core.AbstractVerticle;
import io.vertx.core.AsyncResult;
import io.vertx.core.CompositeFuture;
import io.vertx.core.Future;
import io.vertx.core.eventbus.EventBus;
import io.vertx.core.eventbus.Message;
import io.vertx.core.json.JsonArray;
import io.vertx.core.json.JsonObject;

public class App extends AbstractVerticle {

  private EventBus eb;

  private static String MAX_COUNT_PER_USER = "max_count_per_user";
  private static String MAX_COUNT = "max_count";

  @Override
  public void start() throws Exception {
    eb = vertx.eventBus();

    // consume "/campaigns" events from the event bus
    eb.consumer("/campaigns", this::handleUserRequest);
  }

  /**
   * main caps layer engine
   */
  private void handleUserRequest(Message<JsonObject> message) {
    JsonObject messageJson = message.body();
    String userId = messageJson.getString("userId");

    // get the list of campaigns from the data layer
    Future<JsonArray> campaignListFuture = Future.future();
    eb.send("/data/campaignList", null, reply -> {
      handleReply(reply, campaignListFuture);
    });

    // get the request counters per user
    Future<JsonArray> requestsPerUserFuture = Future.future();
    eb.send("/data/campaignRequestsPerUser",
        new JsonObject().put("userId", userId), reply -> {
      handleReply(reply, requestsPerUserFuture);
    });

    // get the total request counters
    Future<JsonArray> totalRequestsFuture = Future.future();
    eb.send("/data/campaignTotalRequests", null, reply -> {
      handleReply(reply, totalRequestsFuture);
    });

    // when all calls have succeeded, filter the campaigns
    // and return the filtered campaign list
    CompositeFuture.all(
      campaignListFuture, requestsPerUserFuture, totalRequestsFuture)
      .setHandler(futures -> {
        JsonArray campaigns = (JsonArray)futures.result().resultAt(0);
        JsonObject reqPerUser = (JsonObject)futures.result().resultAt(1);
        JsonObject totalReqs = (JsonObject)futures.result().resultAt(2);
        JsonObject counters = new JsonObject()
          .put(MAX_COUNT_PER_USER, reqPerUser)
          .put(MAX_COUNT, totalReqs);
        JsonArray filteredCampaigns = filterCampaigns(userId, campaigns, counters);
        // update the campaign counters
        updateCampaignCounters(userId, filteredCampaigns);
        // return the filtered campaign list
        message.reply(filteredCampaigns);
      });
  }

  @SuppressWarnings("unchecked")
  private <T> void handleReply(
      AsyncResult<Message<Object>> reply, Future<T> future) {
    if (reply.failed()) {
      future.fail(reply.cause());
      return;
    }
    T totalRequests = (T)(reply.result().body());
    future.complete(totalRequests);
  }

  /**
   * filter the campaigns that are below their caps
   */
  private JsonArray filterCampaigns(
      String userId, JsonArray campaigns, JsonObject counters) {
    JsonArray result = new JsonArray();
    for (int i = 0; i < campaigns.size(); i++) {
      JsonObject campaignJsonObj = campaigns.getJsonObject(i);
      if (validateCampaignCaps(userId, campaignJsonObj, counters)) {
        result.add(campaignJsonObj);
      }
    }
    return result;
  }

  /**
   * validate that a campaign is below its caps
   * @param userId the user ID
   * @param campaign the campaign JSON object, which may contain a "caps" field
   * @param counters the counters for this campaign (may be null)
   */
  private boolean validateCampaignCaps(
      String userId, JsonObject campaign, JsonObject counters) {
    String campaignId = campaign.getString("id");
    JsonObject caps = campaign.getJsonObject("caps");
    // requests per user
    Long perUserCap = caps.getLong(MAX_COUNT_PER_USER);
    if (perUserCap != null) {
      // perUserCounts is a JSON objects mapping from campaignId to a number
      JsonObject perUserCounts = counters.getJsonObject(MAX_COUNT_PER_USER);
      Long count = perUserCounts.getLong(campaignId);
      if (count != null && count >= perUserCap) {
        System.out.println("[INFO] Filtering out campaign " + campaignId +
          " - threshold for requests per user of " + perUserCap + " reached.");
        return false;
      }
    }
    // total requests
    Long totalCap = caps.getLong(MAX_COUNT);
    if (totalCap != null) {
      // totalCount is a JSON objects mapping from campaignId to a number
      JsonObject totalCount = counters.getJsonObject(MAX_COUNT);
      Long count = totalCount.getLong(campaignId);
      if (count != null && count >= totalCap) {
        System.out.println("[INFO] Filtering out campaign " + campaignId +
        " - threshold for total requests of " + totalCap + " reached.");
        return false;
      }
    }
    System.out.println("[INFO] Including campaign " + campaignId);
    return true;
  }

  private void updateCampaignCounters(String userId, JsonArray campaigns) {
    JsonArray campaignIdList = new JsonArray();
    for (int i = 0; i < campaigns.size(); i++) {
      campaignIdList.add(campaigns.getJsonObject(i).getString("id"));
    }
    JsonObject payload = new JsonObject()
      .put("userId", userId)
      .put("campaignIdList", campaignIdList);
    eb.send("/data/incrementCounters", payload);
  }

}
