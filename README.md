# Campaign caps - a Walkme exercise

## Modules

To provide scalability and allow other functionality to be added later on, three different modules are created:

1. Http Server: Sevring user requests and handling input and output
2. Campaign-caps-layer: Handling the business logic; filtering campaigns based on the requesting user, campaign thresholds and current request counters
3. Data-layer: Access to data happens here

## What's missing

- The campaign-caps-layer module should enclose a single request inside a transaction, so that the database is immediately updated when a new request comes in.
- The data-layer module should take care of locking the database for a more tighyly-sealed operation in case where more than a single node is deployed.
- In this exercise, all modules appear in the same code-base, which is not a good idea for production code, where each deployed unit should have its own separate code-base.
- Should add tests (currently there's only a single test in the data-layer).

## Running locally

1. Clone the repository
2. Run 'mvn install' in the root folder
3. Open a terminal window for each sub-folder
4. In each terminal window, cd to a sub-folder, and run 'run.sh' (make sure it's executable)

### Example calls:
- http://localhost:8000/campaigns?user_id=user-1
- http://localhost:8000/campaigns?user_id=user-2

### Notes:

- This exercise creates three campaign with their own random caps.
- Each call will update the counters, and return the filtered campaigns for each user.
- To reset the campaign caps, stop and re-run the data-layer module.

## Deployment scenario

- All three modules can be deployed multiple times to allow scaling of the application.
- The modules interact with each other via the Vertx event bus.
- A diagram of a deployment scenario:

```
                     https://.../campaigns?user=123
                                |(8)
                                |(1)
                        ---------------
                        | http-server |
                        ---------------
                                |
                                |(2)
           --------------------------------------------
           |               event bus                  |
           --------------------------------------------
                   |(7)                        |
                   |(3)(5)                     |(4)(6)
           -----------------------       --------------
           | campaign-caps-layer |       | data-layer |
           -----------------------       --------------
```

### In the diagram:

1. User sends an http request
2. http-server sends the request on the event bus
3. campaign-caps-layer consumes the event, and sends a few events to get data
4. data-layer consumes the data requests, and replies with the data
5. campaign-caps layer filters the appropriate campaigns based on userId, request counters and campaign thresholds; it then sends an update data event
6. data-layer consumes the update event, updating the filtered campaigns' request counters
7. campaign-caps-layer replies back to http-server with the filtered list
8. http-server sends back the list of filtered campaigns, sending only the appropriate fields (id and data)
