package com.walkme.exercise;

import com.walkme.exercise.datalayer.DAO.Campaign;
import com.walkme.exercise.datalayer.DAO.CampaignRequests;
import io.vertx.core.json.JsonArray;
import junit.framework.Test;
import junit.framework.TestCase;
import junit.framework.TestSuite;

public class AppTest extends TestCase {
  public AppTest(String testName) {
    super(testName);
  }

  public static Test suite() {
    return new TestSuite(AppTest.class);
  }

  public void testApp() {
    Campaign campaign = Campaign.create("campaign-1", null, null);
    String campaignId = campaign.getId();
    JsonArray campaignIdList = new JsonArray().add(campaignId);

    // no requests
    assertEquals(null, CampaignRequests.getRequestsPerUser("user-1").get(campaignId));
    assertEquals(null, CampaignRequests.getRequestsPerUser("user-2").get(campaignId));
    assertEquals(null, CampaignRequests.getTotalRequests().get(campaignId));

    // request for first user
    CampaignRequests.incrementRequestCount(campaignIdList, "user-1");
    assertEquals(1, (long)CampaignRequests.getRequestsPerUser("user-1").get(campaignId));
    assertEquals(1, (long)CampaignRequests.getTotalRequests().get(campaignId));

    // request for second user
    CampaignRequests.incrementRequestCount(campaignIdList, "user-2");
    assertEquals(1, (long)CampaignRequests.getRequestsPerUser("user-2").get(campaignId));
    assertEquals(2, (long)CampaignRequests.getTotalRequests().get(campaignId));
  }
}
