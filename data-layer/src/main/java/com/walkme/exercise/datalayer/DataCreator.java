package com.walkme.exercise.datalayer;

import com.walkme.exercise.datalayer.DAO.Campaign;
import io.vertx.core.json.JsonObject;
import java.util.HashMap;
import java.util.Map;

/**
 * creates random data and populates the two database classes
 *
 * --> obviously not meant for production... <--
 */
public class DataCreator {

  public static void randomizeData() {
    // create three campaigns
    randomizeCampaign("Popup Ads");
    randomizeCampaign("Marketing - Joe");
    randomizeCampaign("Facebook ongoing");
  }

  private static void randomizeCampaign(String name) {
    long rand = (long)(Math.random() * 10000);
    Map<String, Long> caps = new HashMap<String, Long>();
    if (rand % 2 == 0) {
      caps.put(Campaign.MAX_COUNT, (rand % 10) + 5);
    }
    if (rand % 3 == 0) {
      caps.put(Campaign.MAX_COUNT_PER_USER, (rand % 5) + 2);
    }
    Campaign.create(name, new JsonObject(), caps);
  }
}
