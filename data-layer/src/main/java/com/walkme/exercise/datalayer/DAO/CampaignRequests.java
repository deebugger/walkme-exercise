package com.walkme.exercise.datalayer.DAO;

import io.vertx.core.json.JsonArray;
import java.util.HashMap;
import java.util.Map;

public class CampaignRequests {

  // a static mapping of campaignId to <user => requests> tuples
  // --> replaces a real data source <--
  private static Map<String, Map<String, Long>> __campaignRequests =
    new HashMap<String, Map<String, Long>>();

  /**
   * increment the request count per user, for each campaign in the list
   * return the updated number of user requests to this campaign
   */
  public static void incrementRequestCount(JsonArray campaignIdList, String userId) {
    for (int i = 0; i < campaignIdList.size(); i++) {
      incrementRequestCount(campaignIdList.getString(i), userId);
    }
  }

  private static void incrementRequestCount(String campaignId, String userId) {
    Map<String, Long> userRequestCount = __campaignRequests.get(campaignId);
    if (userRequestCount == null) {
      // first request for this campaign
      userRequestCount = new HashMap<String, Long>();
      userRequestCount.put(userId, 1l);
      __campaignRequests.put(campaignId, userRequestCount);
      return;
    }
    Long requestCount = userRequestCount.get(userId);
    if (requestCount == null) {
      // first request for this user in this campaign
      userRequestCount.put(userId, 1l);
      return;
    }
    // update the request count
    requestCount += 1;
    userRequestCount.put(userId, requestCount);
    return;
  }

  /**
   * return a map of <campaignId to num. requests> for a given user
   * for all campaigns
   */
  public static Map<String, Long> getRequestsPerUser(String userId) {
    Map<String, Long> result = new HashMap<String, Long>();
    for (String campaignId : __campaignRequests.keySet()) {
      Map<String, Long> userRequests = __campaignRequests.get(campaignId);
      Long numRequests = userRequests.get(userId);
      if (numRequests != null) {
        result.put(campaignId, numRequests);
      }
    }
    return result;
  }

  /**
   * return the total number of all requests for all campaigns
   */
  public static Map<String, Long> getTotalRequests() {
    Map<String, Long> totalRequests = new HashMap<String, Long>();
    for (String campaignId : __campaignRequests.keySet()) {
      Map<String, Long> userRequests = __campaignRequests.get(campaignId);
      long total = 0;
      for (Long numRequest : userRequests.values()) {
        total += numRequest;
      }
      totalRequests.put(campaignId, total);
    }
    return totalRequests;
  }

}
