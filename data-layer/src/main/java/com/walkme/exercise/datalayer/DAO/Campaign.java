package com.walkme.exercise.datalayer.DAO;

import io.vertx.core.json.JsonObject;

import java.util.Collection;
import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

public class Campaign {

  public static String MAX_COUNT = "max_count";
  public static String MAX_COUNT_PER_USER = "max_count_per_user";

  // a static list of campaigns
  // --> replaces a real data source <--
  private static Map<String, Campaign> __campaigns =
    new HashMap<String, Campaign>();

  private String id;
  private String name;
  private JsonObject data;
  private Map<String, Long> caps;

  public String getId() { return id; }
  public String getName() { return name; }
  public JsonObject getData() { return data; }
  public Map<String, Long> getCaps() { return caps; }

  private Campaign(String name, JsonObject data, Map<String, Long> caps) {
    this.id = UUID.randomUUID().toString();
    this.name = name;
    this.data = data;
    this.caps = caps == null ? new HashMap<String, Long>() : caps;
  }

  public static Campaign create(
      String name, JsonObject data, Map<String, Long> caps) {
    Campaign campaign = new Campaign(name, data, caps);
    __campaigns.put(campaign.id, campaign);
    return campaign;
  }

  public static Campaign get(String id) {
    return __campaigns.get(id);
  }

  public static Collection<Campaign> list() {
    return __campaigns.values();
  }

  public JsonObject jsonize() {
    JsonObject campaignObj = new JsonObject();
    campaignObj.put("id", id);
    campaignObj.put("name", name);
    campaignObj.put("data", data);
    JsonObject capsObj = new JsonObject();
    for (String key : caps.keySet()) {
      capsObj.put(key, caps.get(key));
    }
    campaignObj.put("caps", capsObj);
    return campaignObj;
  }

  // public boolean validateCaps(String userId) {
  //   if (caps.isEmpty()) { return true; }
  //   // validate max count cap
  //   Long maxCount = caps.get(MAX_COUNT);
  //   if (maxCount != null) {
  //     if (CampaignRequests.getTotalRequests(this.id) >= maxCount) {
  //       System.out.println(">> MAX_COUNT for campaign " + this.id + " reached");
  //       return false;
  //     }
  //   }
  //   // validate max count per user cap
  //   Long maxCountPeruser = caps.get(MAX_COUNT_PER_USER);
  //   if (maxCountPeruser != null) {
  //     if (CampaignRequests.getRequestsPerUser(this.id, userId) >= maxCountPeruser) {
  //       System.out.println(">> MAX_COUNT_PER_USER for user " + userId + " for campaign " + this.id + " reached");
  //       return false;
  //     }
  //   }
  //   return true;
  // }

  // public static List<Campaign> getCampaignList() {
  //   return randomizeCampaigns();
  // }
}
