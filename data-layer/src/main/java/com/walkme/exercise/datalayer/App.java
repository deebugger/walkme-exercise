package com.walkme.exercise.datalayer;

import com.walkme.exercise.datalayer.DAO.Campaign;
import com.walkme.exercise.datalayer.DAO.CampaignRequests;
import io.vertx.core.AbstractVerticle;
import io.vertx.core.eventbus.EventBus;
import io.vertx.core.eventbus.Message;
import io.vertx.core.json.JsonArray;
import io.vertx.core.json.JsonObject;

import java.util.Map;

public class App extends AbstractVerticle {

  @Override
  public void start() throws Exception {
    // randomize the data
    // --> not for production <--
    DataCreator.randomizeData();

    EventBus eb = vertx.eventBus();

    // return a JSON array of campaigns
    eb.consumer("/data/campaignList", this::getCampaignList);

    // return a JSON array of requests per user for all campaigns
    eb.consumer("/data/campaignRequestsPerUser", this::getRequestsPerUser);

    // return a JSON array of total requests for all campaigns
    eb.consumer("/data/campaignTotalRequests", this::getTotalRequests);

    // update a campaign's counters
    eb.consumer("/data/incrementCounters", this::incrementCounters);
  }

  /**
   * reply with a JSON array of campaigns
   */
  public void getCampaignList(Message<Object> message) {
    JsonArray result = new JsonArray();
    for (Campaign campaign : Campaign.list()) {
      result.add(campaign.jsonize());
    }
    message.reply(result);
  }

  /**
   * reply with a JSON object of campaign requests for a given user
   */
  public void getRequestsPerUser(Message<JsonObject> message) {
    JsonObject result = new JsonObject();
    JsonObject messageJson = message.body();
    String userId = messageJson.getString("userId");
    Map<String, Long> requestsPerUser = CampaignRequests.getRequestsPerUser(userId);
    if (requestsPerUser != null) {
      for (String campaignId : requestsPerUser.keySet()) {
        Long reqs = requestsPerUser.get(campaignId);
        result.put(campaignId, reqs == null ? 0 : reqs);
      }
    }
    message.reply(result);
  }

  /**
   * reply with a JSON object of total requests per campaign
   */
  public void getTotalRequests(Message<Object> message) {
    JsonObject result = new JsonObject();
    Map<String, Long> totalRequests = CampaignRequests.getTotalRequests();
    if (totalRequests != null) {
      for (String campaignId : totalRequests.keySet()) {
        Long reqs = totalRequests.get(campaignId);
        result.put(campaignId, reqs == null ? 0 : reqs);
      }
    }
    message.reply(result);
  }

  /**
   * update the counters for the give campaignId and userId
   */
  public void incrementCounters(Message<JsonObject> message) {
    JsonObject messageJson = message.body();
    JsonArray campaignIdList = messageJson.getJsonArray("campaignIdList");
    String userId = messageJson.getString("userId");
    if (campaignIdList != null && userId != null) {
      CampaignRequests.incrementRequestCount(campaignIdList, userId);
    }
  }

}
