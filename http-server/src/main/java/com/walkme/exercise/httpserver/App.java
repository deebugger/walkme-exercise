package com.walkme.exercise.httpserver;

import java.util.UUID;

import io.vertx.core.AbstractVerticle;
import io.vertx.core.eventbus.EventBus;
import io.vertx.core.eventbus.ReplyException;
import io.vertx.core.http.HttpServerResponse;
import io.vertx.core.json.JsonArray;
import io.vertx.core.json.JsonObject;
import io.vertx.ext.web.Router;
import io.vertx.ext.web.RoutingContext;

public class App extends AbstractVerticle {

  private EventBus eb;

  @Override
  public void start() throws Exception {
    Router router = Router.router(vertx);
    eb = vertx.eventBus();

    // handle /campaigns
    router.get("/campaigns").handler(this::handleCampaignCaps);

    // handle everything else
    router.route().handler(routingContext -> {
      sendError(routingContext.response(), 400,
        new JsonObject().put("error", "Not a valid URL"));
    });

    vertx.createHttpServer().requestHandler(router::accept).listen(8000);
  }

  /**
   * validate user_id parameter and send to event bus
   */
  private void handleCampaignCaps(RoutingContext routingContext) {
    String callId = UUID.randomUUID().toString();
    System.out.println("[INFO] " + callId + " /campaigns called");
    String userId = routingContext.request().getParam("user_id");
    if (userId == null) {
      System.out.println("[ERROR] " + callId + " no user_id parameter");
      sendError(routingContext.response(), 400,
        new JsonObject().put("error", "Missing user_id"));
      return;
    }
    eb.send("/campaigns",
      new JsonObject().put("userId", userId).put("callId", callId),
      reply -> {
        if (reply.succeeded()) {
          System.out.println("[INFO] " + callId + " good response from event bus");
          JsonArray responseArray = getOutputData((JsonArray)reply.result().body());
          routingContext.response()
            .putHeader("content-type", "application/json")
            .end(responseArray.toString());
        } else {
          ReplyException ex = (ReplyException)reply.cause();
          JsonObject error = new JsonObject().put("type", ex.failureType())
            .put("code", ex.failureCode()).put("message", ex.getMessage());
          System.out.println("[ERROR] " + callId +
            " bad response from event bus: " + reply.cause().getMessage());
          sendError(routingContext.response(), 500,
            new JsonObject().put("error", error));
        }
      });
  }

  /**
   * only return certain data to the user
   */
  private JsonArray getOutputData(JsonArray campaigns) {
    JsonArray result = new JsonArray();
    for (int i = 0; i < campaigns.size(); i++) {
      result.add(new JsonObject()
        .put("id", campaigns.getJsonObject(i).getString("id"))
        .put("data", campaigns.getJsonObject(i).getJsonObject("data")));
    }
    return result;
  }

  /**
   * standard error to caller
   */
  private void sendError(HttpServerResponse response, int statusCode,
      JsonObject message) {
    response
      .putHeader("content-type", "application/json")
      .setStatusCode(statusCode)
      .end(message.toString());
  }
}
